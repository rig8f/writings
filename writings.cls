% (C) 2018-19 Filippo Rigotto.
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{writings}[2019/03/08 writings 1.0.3, a LaTeX class for notes and reports]
\RequirePackage{xkeyval}

%%%%%

% class options
\newif\ifnopagenum\nopagenumfalse
\newif\iflibertine\libertinefalse
\newif\ifpalatino\palatinofalse
\newif\ifgaramond\garamondfalse
\newif\ifwatermarks\watermarksfalse
\newif\ifdraft\draftfalse
\newif\ifconfidential\confidentialfalse
\newif\ifcc\ccfalse
\newif\ifccfoot\ccfootfalse

\DeclareOption{nopagenum}{\nopagenumtrue}
\DeclareOption{libertine}{\libertinetrue}
\DeclareOption{palatino}{\palatinotrue}
\DeclareOption{garamond}{\garamondtrue}
\DeclareOption{draft}{\watermarkstrue\drafttrue}
\DeclareOption{confidential}{\watermarkstrue\confidentialtrue}
\DeclareOption{ccfootnote}{\cctrue\ccfoottrue}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions\relax

% key-value for license type
\newcommand{\cctype}{by-nc-nd}
\DeclareOptionX{cclicense}{\cctrue\renewcommand{\cctype}{#1}}
\ProcessOptionsX\relax

% build upon base class
\LoadClass[a4paper,11pt,oneside]{article}

%%%%%

% packages
\RequirePackage[utf8]{inputenc}
\RequirePackage{lmodern}
\iflibertine\RequirePackage{libertine}\fi
\ifpalatino\RequirePackage{mathpazo}\fi
\ifgaramond\RequirePackage{Cormorantgaramond}\fi
\RequirePackage{textcomp}
\RequirePackage{amssymb,amsmath,amsthm}
\RequirePackage{marvosym,fontawesome5,awesomebox}
\RequirePackage[T1]{fontenc}
\RequirePackage{babel,varioref}

\RequirePackage[margin=2.5cm]{geometry}
\RequirePackage{etoolbox,xstring,parskip}
\ifgaramond\else\RequirePackage{microtype}\fi % not supported in garamond
\RequirePackage{xcolor,graphicx,subfig,float}
\RequirePackage{tabularx,booktabs,paralist}
\RequirePackage{multicol,epigraph}

\RequirePackage{csquotes}
\RequirePackage[backend=biber,style=ieee]{biblatex}
\ifwatermarks\RequirePackage[printwatermark]{xwatermark}\fi
\ifcc\RequirePackage[%
	type={CC}, modifier={\cctype}, version={4.0},%
	imagemodifier={-eu}, imageposition={left}, imagedistance={1.5em}
]{doclicense}\fi
\RequirePackage{hyperref}
\definecolor{dkpowder}{rgb}{0,0.2,0.7}
\hypersetup{%
    pdfpagemode  = {UseOutlines},
    bookmarksopen,
    pdfstartview = {FitH},
    colorlinks,
    linkcolor = {black},
    citecolor = {black},
    urlcolor  = {dkpowder},
}

% page numbering
\ifnopagenum\pagenumbering{gobble}\fi

%%%%%

% unmarked footnote
\newcommand\footnoteblank[1]{%
    \begingroup
    \renewcommand\thefootnote{}\footnote{#1}%
    \addtocounter{footnote}{-1}%
    \endgroup
}

%%%%%

% watermarks
\ifconfidential\newwatermark[allpages,color=black!25,angle=45,%
    scale=3,xpos=-10,ypos=10]{CONFIDENTIAL}\fi
\ifdraft\newwatermark[allpages,color=black!25,angle=45,%
    scale=4,xpos=-10,ypos=40]{DRAFT}\fi

%%%%%

% mail extractor
\DeclareRobustCommand{\showMail}[1]{%
    \IfSubStr{#1}{@}%
    {\StrSubstitute{#1}{@}{ AT }}%
    {#1}%
}

% site extractor
\newcommand*{\xStrDel}{\expandafter\StrDel\expandafter}
\DeclareRobustCommand{\showSite}[1]{
	\saveexpandmode\noexpandarg
	\def\res{#1}%
	\xStrDel{\res}{https://}[\res]%
	\xStrDel{\res}{http://}[\res]%
	\xStrDel{\res}{www.}[\res]%
	\res\restoreexpandmode
}

% document authors' info as footnote
\newcommand{\authorinfo}[3]{%
    #1\footnote{~\faGlobeAfrica\href{#2}{\texttt{\showSite{#2}}}%
    ~--~\faEnvelope~~\href{mailto:#3}{\texttt{\showMail{#3}}}%
}}

%%%%%

% margin symbols
\newlength{\mglen}
\setlength{\mglen}{0.7cm} % margin distance
\newcommand{\leftsym}[1]{%
	\hspace*{-\mglen}%
	\makebox[0pt][r]{#1}%
	\hspace*{\mglen}%
}

\newcommand{\code} {\leftsym{\large\faCode}}
\newcommand{\hand} {\leftsym{\large\faHandPointRight}}
\newcommand{\info} {\leftsym{\large\faInfoCircle}}
\newcommand{\gear} {\leftsym{\large\faCogs}}
\newcommand{\warn} {\leftsym{\large\faExclamationTriangle}}
\newcommand{\cafe} {\leftsym{\Large\Coffeecup}}
\newcommand{\rad}  {\leftsym{\Large\Radioactivity}}
\newcommand{\haz}  {\leftsym{\Large\Biohazard}}

%%%%%

% start chapters quotations
% from https://github.com/derric/cleanthesis
\definecolor{lightgray}{gray}{.8}
\newcommand{\hugequote}{%
	{\fontfamily{pbk}\fontseries{m}\fontsize{75}{80}\selectfont%
	\hspace*{-.475em}\color{lightgray}%
	\textit{\glqq}%
	\vskip -.26em}%
}
\newcommand{\startquote}[3]{%
	\begin{minipage}{.865\textwidth}%
		\begin{flushright}
			\begin{minipage}{.65\textwidth}%
				%\begin{flushleft}
					{\hugequote}\noindent\textit{#1}
				%\end{flushleft}
	    		\begin{flushright}
    				--- \textbf{#2} \\
    				#3
		    	\end{flushright}
		    \end{minipage}%
		\end{flushright}
	\end{minipage}%
	\bigskip
}

%%%%%

% maketitle redefined
\renewcommand{\and}{\hspace{2em}}
\renewcommand{\maketitle}{%
\begin{center}\Large%
    \textbf{\LARGE\@title}\\[1em]
    \@author\vspace*{1em}%
    \ifccfoot\footnoteblank{\doclicenseIcon\doclicenseText}\fi%
\end{center}}

\frenchspacing