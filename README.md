# Writings

A LaTeX class to typeset notes and reports.

For a short guide, an output example and the class documentation see [the PDF](writings-guide.pdf).

---
(C) 2018-19 Filippo Rigotto. Subject to [BSD-3 license](LICENSE).