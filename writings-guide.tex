% arara: pdflatex
% arara: pdflatex
\documentclass{writings}

\hypersetup{%
    pdfauthor   = {Filippo Rigotto},
    pdftitle    = {Writings - Filippo Rigotto},
    pdfsubject  = {Writings class - a short guide},
    pdfkeywords = {},
    pdfproducer = {LaTeX},
    pdfcreator  = {pdfLaTeX}
}

\title{\textit{Writings} class -- a short guide}
\author{\authorinfo{Filippo Rigotto}{https://rig8f.ml}{rigottofilippo@altervista.org}}
\begin{document}
    \maketitle
    The class \texttt{writings}\footnote{~This document refers to \texttt{writings} 1.0.3, version of Mar. 8, 2019.} helps typesetting notes and reports, and is tailored to the academical use of its author.
    It includes all the usual packages for figures, tables and bibliography, plus special features and macros, such as watermarks, different fonts and Creative Commons licenses settings.

    \section{Class options}
    The class is based on the official \texttt{article}:
    all unrecognized options will be passed to it, so all the options proper to \texttt{article} will work.
    \texttt{writings} loads by default with \texttt{a4paper}, \texttt{11pt} and \texttt{oneside} options.

    \warn Please note that two-side mode is \textit{not} tested and may lead to ugly results.

    The class supports many (boolean) options and one key-value option to select the CC license type, all presented in Table~\ref{t:opt}.

    \begin{table}[ht]
        \centering
        \caption{Class options}\label{t:opt}
        \begin{tabular}{ll}
            \toprule
            Option                & Description \\ \midrule
            \texttt{nopagenum}    & Excludes page numbering. Useful for 1-page documents. \\
            \texttt{libertine}    & Loads Libertine fonts instead of the standard Latin Modern. \\
            \texttt{palatino}     & Loads Palatino fonts instead of the standard Latin Modern. \\
            \texttt{garamond}     & Loads Cormorant Garamond fonts instead of the standard Latin Modern. \\
            \texttt{draft}        & Applies a grey diagonal DRAFT watermark to all pages. \\
            \texttt{confidential} & Applies a grey diagonal CONFIDENTIAL watermark to all pages. \\
            \texttt{ccfootnote}   & Adds a blank footnote with a short version of the chosen CC license. \\
            \texttt{cclicense}    & Selects the CC license for the document: defaults to \verb!by-nc-nd! but also \\ 
                                  & \verb!by!, \verb!by-sa!, \verb!by-nd!, \verb!by-nc!, \verb!by-nc-sa! are accepted values. \\ \bottomrule
        \end{tabular}        
    \end{table}

    \section{Included macros}
    There are several included macros to typeset specific things.

    \verb!\footnoteblank{text}! adds an unnumbered footnote at the bottom of the current page.

    \verb!\authorinfo{name}{website}{mail}! is used in the \verb!\author! macro to typeset the name, and a footnote containing a properly stripped version of the website and e-mail of the author.
    Empty values are allowed but not properly handled yet.

    Quotes at the beginning of chapters and sections may be inserted using the \textit{epigraph} package or via the \verb!\startquote{text}{author}{info}! command, borrowed and adapted from \textit{CleanThesis}\footnote{~\url{https://github.com/derric/cleanthesis}} styles.
    An example of the output of this command is provided below.

    \startquote{We have seen that computer programming is an art, because it applies accumulated knowledge to the world, because it requires skill and ingenuity, and especially because it produces objects of beauty.}{D. E. Knuth}{(CS Professor)}

    Finally, a set of commands is provided to add special symbols to the left of a paragraph.
    These symbols are from \textit{fontawesome5} and \textit{marvosym} icon sets:
    the use of both packages is dictated by the former not having a few crucial symbols.
    The correct way of using them shown in Table~\ref{t:sym}: right before starting the paragraph, followed by a space.
    If a box around the content is needed, then use commands provided by the \textit{awesomebox} package instead, as the example below demonstrates.

    \importantbox{Be sure to download the latest version of the \textit{awesomebox} package from CTAN for full \LaTeX{} compatibility, because older versions only work with Xe\LaTeX{} and Lua\LaTeX.}

    \begin{table}[ht]
        \centering
        \caption{List of left-symbols commands}\label{t:sym}
        \begin{tabular}{cl}
            \toprule
            Symbol                 & Command \\ \midrule
            \faCode                & \verb!\code! Lorem ipsum dolor sit amet, \\
            \faHandPointRight      & \verb!\hand! consectetur adipiscing elit, \\
            \faInfoCircle          & \verb!\info! sed do eiusmod tempor incididunt \\
            \faCogs                & \verb!\gear! ut labore et dolore magna aliqua. \\
            \faExclamationTriangle & \verb!\warn! Ut enim ad minim veniam, \\
            \Coffeecup             & \verb!\cafe! quis nostrud exercitation \\
            \Radioactivity         & \verb!\rad! ullamco laboris nisi ut aliquip \\
            \Biohazard             & \verb!\haz! ex ea commodo consequat. \\ \bottomrule
        \end{tabular}
    \end{table}

    \section{More on license settings}
    Licenses are provided by the \textit{doclicense} package.
    It is loaded whenever the \verb!cclicense! option has been given a meaningful value.
    Apart from that, a blank footnote with a short version of the license may be automatically added using the \verb!ccfootnote! option.
    All package's macros are callable in the document, so the author may choose not to display a footnote to dedicate a full section/paragraph to the license, using the package-provided \verb!\doclicenseThis! macro.

    \appendix
    \section{Used packages}
    This class relies on a lot of packages that are usually of common use, or that are internally needed.
    Table~\ref{t:pck} shows the full list of packages, grouped by topic.

    \begin{table}[ht]
        \centering
        \caption{Packages}\label{t:pck}
        \begin{tabular}{cl}
            \toprule
            Type          & Packages \\ \midrule
            Internals     & \verb!inputenc!, \verb!fontenc!, \verb!etoolbox!, \verb!xstring! \\
            Fonts         & \verb!lmodern!, \verb!libertine!, \verb!mathpazo!, \verb!Cormorantgaramond!, \verb!textcomp! \\
            Maths         & \verb!amssymb!, \verb!amsmath!, \verb!amsthm! \\
            Symbols       & \verb!fontawesome5!, \verb!marvosim!, \verb!awesomebox! \\
            I18n          & \verb!babel!, \verb!varioref! \\
            Typesetting   & \verb!geometry!, \verb!parskip!, \verb!microtype!, \verb!multicol!, \verb!epigraph! \\
            Figures       & \verb!xcolor!, \verb!graphicx!, \verb!subfig!, \verb!float! \\
            Tables        & \verb!tabularx!, \verb!booktabs! \\
            Bibliography  & \verb!csquotes!, \verb!biblatex! \\
            Other         & \verb!hyperref!, \verb!xwatermark!, \verb!doclicense! \\ \bottomrule
        \end{tabular}
    \end{table}
\end{document}